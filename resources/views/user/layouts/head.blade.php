<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>camelCase</title>

    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('user/css/coolblue.css')}}" />

    <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
    <script src="{{asset('user/js/jquery-1.6.1.min.js')}}"></script>

    <script src="{{asset('user/js/scrollToTop.js')}}"></script>

</head>