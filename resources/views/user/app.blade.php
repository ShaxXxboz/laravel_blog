<!DOCTYPE html>
<html lang="en">


  @include('user/layouts/head')

  <body>

    @include('user/layouts/header')

    
    @section('main-content')

           @show

           
    <!-- Footer -->
    @include('user/layouts/footer')

  </body>

</html>
