@extends('user/app')

@section('bg-img', asset('user/img/post-bg.jpg'))
@section('title', 'Post Title')
@section('sub-heading', 'Post Subheading')


@section('main-content')

<div id="content-wrap">

    <!-- content -->
    <div id="content" class="clearfix">

        <!-- main -->
        <div id="main">

            <!-- post -->
            <article class="post single">

                <!-- primary -->
              <div class="primary">

                  <h2><a href="index.html">A Blog Post</a></h2>

                    <p class="post-info"><span>Filed under</span> <a href="index.html">templates</a>, <a href="index.html">internet</a></p>

                    <div class="image-section">
                      <img src="{{asset('user/images/img-post.jpg')}}" alt="image post" height="206" width="498"/>
                  </div>

            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.
          Cras id urna. Morbi tincidunt, orci ac <a href="index.html">convallis aliquam</a>, lectus turpis varius lorem, eu
            posuere nunc justo tempus leo.</p>

            <p>
            Donec mattis, purus nec placerat bibendum, <a href="index.html">dui pede condimentum</a>
            odio, ac blandit ante orci ut diam. Cras fringilla magna. Phasellus suscipit, leo a pharetra
            condimentum, lorem tellus eleifend magna, <a href="index.html">eget fringilla velit</a> magna id neque. Curabitur vel urna.
            In tristique orci porttitor ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.
            Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu
            posuere nunc justo tempus leo.</p>

                <!-- /primary -->
                </div>


            <aside>

              <p class="dateinfo">JAN<span>31</span></p>

                <div class="post-meta">
                    <h4>Post Info</h4>
                    <ul>
                        <li class="user"><a href="#">Erwin</a></li>
                        <li class="time"><a href="#">12:30 PM</a></li>
                        <li class="comment"><a href="#">10 Comments</a></li>
                        <li class="permalink"><a href="#">Permalink</a></li>
                    </ul>
                </div>

               <div class="post-meta">
                    <h4>tags</h4>
          <ul class="tags">
                <li><a href="index.html" rel="tag">Clean</a></li>
                  <li><a href="index.html" rel="tag">Blog</a></li>
                  <li><a href="index.html" rel="tag">Minimal</a></li>
               </ul>
               </div>

            </aside>

        <!-- /post -->
        </article>

        <!-- post-bottom-section -->
        <div class="post-bottom-section">

        <h4>3 comments</h4>

            <div class="primary">

              <ol class="commentlist">

            <li class="depth-1">

              <div class="comment-info">
                <img alt="" src="{{asset('user/images/gravatar.jpg')}}" class="avatar" height="42" width="42" />
                <cite>
                  <a href="index.html">Erwin</a> Says: <br />
                  <span class="comment-data"><a href="#comment-63" title="">January 31st, 2010 at 10:00 pm</a></span>
                </cite>
              </div>

              <div class="comment-text">
                <p>Comments are great!</p>

                <div class="reply">
                  <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                </div>
              </div>

              <ul class="children">

                <li class="depth-2">

                  <div class="comment-info">
                    <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                    <cite>
                      <a href="index.html">Erwin</a> Says: <br />
                      <span class="comment-data"><a href="#" title="">January 31st, 2010 at 8:15 pm</a></span>
                    </cite>
                  </div>

                  <div class="comment-text">
                    <p>And here is a threaded comment.</p>
                    <div class="reply">
                      <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                    </div>
                  </div>

                </li>

                <li class="depth-2">

                  <div class="comment-info">
                    <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                    <cite>
                      <a href="index.html">Erwin</a> Says: <br />
                      <span class="comment-data"><a href="#" title="">January 31st, 2010 at 8:15 pm</a></span>
                    </cite>
                  </div>

                  <div class="comment-text">
                    <p>And here is another threaded comment.</p>

                    <div class="reply">
                      <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                    </div>
                  </div>

                  <ul class="children">

                    <li class="depth-3">

                      <div class="comment-info">
                        <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                        <cite>
                          <a href="index.html">Erwin</a> Says: <br />
                          <span class="comment-data"><a href="#" title="">January 31st, 2010 at 8:10 pm</a></span>
                        </cite>
                      </div>

                      <div class="comment-text">
                        <p>Threaded comments are cool!</p>

                        <div class="reply">
                          <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                        </div>
                      </div>

                    </li>

                  </ul>

                </li>

              </ul>

            </li>

            <li class="thread-alt depth-1">

              <div class="comment-info">
                <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                <cite>
                  <a href="index.html">Erwin</a> Says: <br />
                  <span class="comment-data"><a href="#comment-63" title="">January 31st, 2010 at 8:00 pm</a></span>
                </cite>
              </div>

              <div class="comment-text">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero.
                Suspendisse bibendum.</p>

                <div class="reply">
                  <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                </div>
              </div>

              <ul class="children">

                <li class="depth-2">

                  <div class="comment-info">
                    <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                    <cite>
                      <a href="index.html">Erwin</a> Says: <br />
                      <span class="comment-data"><a href="#" title="">January 31st, 2010 7:35 pm</a></span>
                    </cite>
                  </div>

                  <div class="comment-text">
                    <p>Donec libero. Suspendisse bibendum.</p>

                    <div class="reply">
                      <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                    </div>
                  </div>

                           <ul class="children">

                    <li class="depth-3">

                      <div class="comment-info">
                        <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                        <cite>
                          <a href="index.html">Erwin</a> Says: <br />
                          <span class="comment-data"><a href="#" title="">January 31st, 2010 at 7:20 pm</a></span>
                        </cite>
                      </div>

                      <div class="comment-text">
                        <p>Threaded comments are cool!</p>

                        <div class="reply">
                          <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                        </div>
                      </div>

                    </li>

                  </ul>



                </li>

              </ul>

            </li>

            <li class="depth-1">

              <div class="comment-info">
                <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                <cite>
                  <a href="index.html">Erwin</a> Says: <br />
                  <span class="comment-data"><a href="#comment-63" title="">January 31st, 2010  at 6:08 pm</a></span>
                </cite>
              </div>

              <div class="comment-text">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.
                Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu
                posuere nunc justo tempus leo.</p>

                <div class="reply">
                  <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                </div>

              </div>

              <ul class="children">

                <li class="depth-2">

                  <div class="comment-info">
                    <img alt="" src="images/gravatar.jpg" class="avatar" height="42" width="42" />
                    <cite>
                      <a href="index.html">Erwin</a> Says: <br />
                      <span class="comment-data"><a href="#comment-63" title="">January 31st, 2010 at 6:08 pm</a> </span>
                    </cite>
                  </div>

                  <div class="comment-text">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

                    <div class="reply">
                      <a rel="nofollow" class="comment-reply-link" href="index.html">Reply</a>
                    </div>
                  </div>

                </li>

              </ul>

            </li>

                <!-- /comment-list -->
        </ol>

            <!-- /primary -->
            </div>

         </div>

         <div class="post-bottom-section">

        <h4>Leave a Reply</h4>

            <div class="primary">

              <form action="index.html" method="post" id="commentform">

                    <div>
              <label for="name">Name <span>*</span></label>
            <input id="name" name="name" value="Your Name" type="text" tabindex="1" />
          </div>
                    <div>
            <label for="email">Email Address <span>*</span></label>
            <input id="email" name="email" value="Your Email" type="text" tabindex="2" />
          </div>
                    <div>
            <label for="website">Website</label>
            <input id="website" name="website" value="Your Website" type="text" tabindex="3" />
          </div>
                    <div>
            <label for="message">Your Message <span>*</span></label>
            <textarea id="message" name="message" rows="10" cols="18" tabindex="4"></textarea>
          </div>
                    <div class="no-border">
              <input class="button" type="submit" value="Submit Comment" tabindex="5" />
          </div>

               </form>

            </div>

         </div>

        <!-- /main -->
        </div>

        <!-- sidebar -->
    <div id="sidebar">

            <div class="about-me">

              <h3>About Me</h3>

                <p>
            <a href="index.html"><img src="images/gravatar.jpg" width="42" height="42" alt="firefox" class="align-left" /></a>
                Lorem ipsum dolor sit, consectetuer adipiscing. Donec libero. Suspendisse bibendum.
          Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu
          posuere nunc justo tempus leo suspendisse bibendum. <a href="index.html">Learn more...</a>
          </p>

            </div>

      <div class="sidemenu">

        <h3>Sidebar Menu</h3>
                <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="index.html#TemplateInfo">TemplateInfo</a></li>
          <li><a href="style.html">Style Demo</a></li>
          <li><a href="blog.html">Blog</a></li>
          <li><a href="archives.html">Archives</a></li>
          <li><a href="http://themeforest.net?ref=ealigam" title="Web Templates">Web Templates</a></li>
        </ul>

      </div>

      <div class="sidemenu">

        <h3>Sponsors</h3>

                <ul>
              <li><a href="http://themeforest.net?ref=ealigam" title="Site Templates">Themeforest
                <span>Site Templates, Web &amp; CMS Themes.</span></a>
                  </li>
          <li><a href="http://www.4templates.com/?go=228858961" title="Website Templates">4Templates
                <span>Low Cost High-Quality Templates.</span></a>
                  </li>
          <li><a href="http://store.templatemonster.com?aff=ealigam" title="Web Templates">Templatemonster
                <span>Delivering the Best Templates on the Net!</span></a>
                  </li>
          <li><a href="http://graphicriver.net?ref=ealigam" title="Stock Graphics">Graphic River
                <span>Awesome Stock Graphics.</span></a>
                  </li>
                    <li><a href="http://www.dreamhost.com/r.cgi?287326|sshout" title="Webhosting">Dreamhost
                <span>Premium Webhosting. Use the promocode <strong>sshout</strong> and save <strong>50 USD</strong>.</span></a>
                  </li>
        </ul>

      </div>

            <div class="sidemenu popular">

        <h3>Most Popular</h3>
        <ul>
          <li><a href="index.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            <span>Posted on December 22, 2010</span></a>
          </li>
                <li><a href="index.html">Cras fringilla magna. Phasellus suscipit.
            <span>Posted on December 20, 2010</span></a>
          </li>
                <li><a href="index.html">Morbi tincidunt, orci ac convallis aliquam.
            <span>Posted on December 15, 2010</span></a>
          </li>
                <li><a href="index.html">Ipsum dolor sit amet, consectetuer adipiscing elit.
            <span>Posted on December 14, 2010</span></a>
          </li>
                <li><a href="index.html">Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem
            <span>Posted on December 12, 2010</span></a>
          </li>
        </ul>

      </div>

        <!-- /sidebar -->
    </div>

    <!-- content -->
  </div>

<!-- /content-out -->
</div>


@endsection